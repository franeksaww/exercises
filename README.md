## Skychallenge

### chapter I
Requirements
- python3 
- pytest

How to run?
- open terminal
- go to car.py file location
- run command: pytest

### chapter II
Requirements:
- python3
- postgresql 11

How to run?
- in file tasks.py add yours postgresql user data (line 9)
- open terminal in tasks.py location
- run python3 tasks.py create_db confirm
- enjoy program using commands add, update, remove, list

### chapter III
Requirements
- python3

How to run?
- open terminal
- go to find_numbers.py file location
- run python3 find_numbers.py
- program will return total number of founded numbers
