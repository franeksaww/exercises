from car import Car, IllegalCarError


def test_car_valid_data():
    c = Car(pax_count=3, car_mass=2000, gear_count=4)
    assert c.total_mass() == 2000 + 3*70


def test_car_mass_to_big():
    try:
        Car(pax_count=3, car_mass=2100, gear_count=4)
    except IllegalCarError as err:
        assert err.args[0] == "Car is to heavy!"


def test_car_mass_to_low():
    try:
        Car(pax_count=3, car_mass=0, gear_count=4)
    except IllegalCarError as err:
        assert err.args[0] == "Car has no mass!"


def test_car_pax_to_low():
    try:
        Car(pax_count=0, car_mass=2000, gear_count=4)
    except IllegalCarError as err:
        assert err.args[0] == "Pax count is not valid!" \
                              "It has to be larger than 0 and less than 6"


def test_car_pax_to_large():
    try:
        Car(pax_count=6, car_mass=2000, gear_count=4)
    except IllegalCarError as err:
        assert err.args[0] == "Pax count is not valid!" \
                              "It has to be larger than 0 and less than 6"
