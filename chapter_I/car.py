class IllegalCarError(Exception):
    pass


class Car:
    def __init__(self, pax_count, car_mass, gear_count):
        self.pax_count = pax_count
        self.car_mass = car_mass
        self.gear_count = gear_count

    def total_mass(self):
        return self.pax_count * 70 + self._car_mass

    @property
    def car_mass(self):
        return self._car_mass

    @car_mass.setter
    def car_mass(self, new_car_mass):
        if 0 < new_car_mass <= 2000:
            self._car_mass = new_car_mass
        else:
            if new_car_mass > 2000:
                raise IllegalCarError("Car is to heavy!")
            else:
                raise IllegalCarError("Car has no mass!")

    @property
    def pax_count(self):
        return self._pax_count

    @pax_count.setter
    def pax_count(self, new_pax_count):
        if 1 <= new_pax_count & new_pax_count <= 5:
            self._pax_count = new_pax_count
        else:
            raise IllegalCarError("Pax count is not valid!"
                                  "It has to be larger than 0 and less than 6")


if __name__ == '__main__':
    c = Car(pax_count=3, car_mass=2000, gear_count=4)
    print(c.total_mass())
