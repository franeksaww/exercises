def find_numbers():
    counter = 0
    for number in range(372 ** 2, 809 ** 2):
        check_increase = 0
        pairs_count = 0
        given_pairs = []
        digits = [int(x) for x in str(number)]
        for i in range(len(digits)-1):
            if digits[i] > digits[i+1]:
                check_increase = 1
                break
            else:
                if digits[i] == digits[i+1] and digits[i] not in given_pairs:
                    given_pairs.append(digits[i])
                    pairs_count += 1
        if pairs_count >= 2 and check_increase == 0:
            counter += 1
    return counter


if __name__ == "__main__":
    print(find_numbers())
