import argparse
from datetime import date

from task_db_utils import SqlUtils


class Tasks:
    def __init__(self):
        self.db_utils = SqlUtils(user='sampleusername',
                                 host='samplehost', password='superpassword')

    def show_all(self, search_type):
        sql = "select * from task_list"
        tasks = self.db_utils.execute_sql(sql_code=sql)
        if tasks:
            if search_type == 0:
                for row in tasks:
                    self.display_tasks(row)
            if search_type == 1:
                for row in tasks:
                    if str(row['date']).split(' ')[0] == str(date.today()):
                        self.display_tasks(row)
        else:
            print('There are no tasks so far!')

    def update_task(self, hash_u, update_data):
        sql = f"select from task_list WHERE hash = '{hash_u}'"
        task = self.db_utils.execute_sql(sql_code=sql)
        if task:
            sql = f"UPDATE task_list SET {update_data} WHERE hash = '{hash_u}'"
            self.db_utils.execute_sql(sql_code=sql)
            print(f'Successfully updated task {hash_u}')
        else:
            print(f'Task with hash {hash_u} does not exists')

    def insert(self, data):
        self.db_utils.insert_data(data=data)
        print('Successfully added task')

    def remove(self, hash_r):
        sql = f"select from task_list WHERE hash = '{hash_r}'"
        task = self.db_utils.execute_sql(sql_code=sql)
        if task:
            sql = f"delete from task_list WHERE hash = '{hash_r}'"
            self.db_utils.execute_sql(sql_code=sql)
            print(f'Successfully deleted task {hash_r}')
        else:
            print(f'Task with hash {hash_r} does not exists')

    def configure(self):
        self.db_utils.create_db()
        sql = """
        CREATE TABLE task_list (
            hash VARCHAR (50) UNIQUE NOT NULL,
            name VARCHAR (264) NOT NULL,
            description TEXT,
            date TIMESTAMP
        );
        """
        self.db_utils.execute_sql(sql_code=sql)
        print('Successfully setup database')

    @staticmethod
    def display_tasks(row):
        print("Hash = ", row['hash'])
        print("Name = ", row['name'])
        print("Description = ", row['description'])
        print("Deadline  = ", row['date'], "\n")


if __name__ == '__main__':
    a = Tasks()
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='commands')

    # list commands
    list_parser = subparsers.add_parser('list', help='show all tasks')
    list_parser.add_argument('--today',
                             action='store_true', help='tasks for today')
    list_parser.add_argument('--all',
                             action='store_true', help='tasks for today')

    # create commands
    create_parser = subparsers.add_parser('add', help='Create a task')
    create_parser.add_argument('name', action='store', help='Task name')
    create_parser.add_argument('description',
                               action='store', help='Task description')
    create_parser.add_argument('deadline',
                               action='store',
                               help='Task deadline(YYYY-MM-DD H:M:S)')

    # delete commands
    delete_parser = subparsers.add_parser('remove', help='Remove task')
    delete_parser.add_argument('hash_r',
                               action='store', help='Hash task to remove')

    # configure commands
    configure_parser = subparsers.add_parser('create_db',
                                             help='Create a database')
    configure_parser.add_argument('confirm', help='confirm')

    # update commands
    update_parser = subparsers.add_parser('update', help='Update task')
    update_parser.add_argument('hash_u', action='store', help='Task hash')
    update_parser.add_argument('--name_u', action='store', help='Task name')
    update_parser.add_argument('--description_u',
                               action='store', help='Task description')
    update_parser.add_argument('--deadline_u',
                               action='store',
                               help='Task deadline(YYYY-MM-DD H:M:S)')

    args = parser.parse_args()
    try:
        if args.name:
            hashed_name = str(hash(vars(args)['name']))[:5]
            data = [hashed_name, vars(args)['name'],
                    vars(args)['description'], vars(args)['deadline']]
            a.insert(tuple(data))
    except AttributeError:
        pass
    try:
        if args.today:
            a.show_all(search_type=1)
    except AttributeError:
        pass
    try:
        if args.all:
            a.show_all(search_type=0)
    except AttributeError:
        pass
    try:
        if args.confirm:
            a.configure()
    except AttributeError:
        pass
    try:
        if args.hash_r:
            a.remove(hash_r=vars(args)['hash_r'])
    except AttributeError:
        pass
    try:
        if args.hash_u:
            data = ''
            name = vars(args)['name_u']
            deadline = vars(args)['deadline_u']
            description = vars(args)['description_u']
            if name:
                data += f"name = '{name}',"
            if deadline:
                data += f"date = '{deadline}',"
            if description:
                data += f"description = '{description}',"
            a.update_task(hash_u=vars(args)['hash_u'], update_data=data[:-1])
    except AttributeError:
        pass
