import re

from psycopg2 import OperationalError, connect
from psycopg2.errors import DuplicateDatabase
from psycopg2.extras import RealDictCursor


class SqlUtils:
    def __init__(self, user, host, password):
        self.user = user
        self.host = host
        self.password = password

    def create_db(self):
        """
        Create db tasks.
        """
        try:
            conn = self.create_connection(None, )
            crs = conn.cursor()
            crs.execute('CREATE database tasks')
            crs.close()
            conn.close()
        except OperationalError:
            print('Connection erorr')
        except DuplicateDatabase:
            print('Database already exist!')

    def execute_sql(self, sql_code):
        """
        Run given sql code with psycopg2.
        :param str sql_code: sql code to run
        :param str db: name of db,
        :rtype: list
        :return: data from psycobg2 cursor as a list (can be None),
         if nothing to fetch.
        """
        try:
            select_query = False
            re_result = re.search(r'select', sql_code, re.I)
            if re_result:
                select_query = True
            conn = self.create_connection(db='tasks', )
            conn.autocommit = True
            if select_query:
                crs = conn.cursor(cursor_factory=RealDictCursor)
            else:
                crs = conn.cursor()
            crs.execute(sql_code)
            if select_query:
                result_tmp = crs.fetchall()
            else:
                result_tmp = None
            crs.close()
            conn.close()
            return result_tmp
        except OperationalError:
            print('Connection erorr')
        except DuplicateDatabase:
            print('Database already exist!')

    def create_connection(self, db, autocommit=True):
        if db:
            conn = connect(
                user=self.user,
                password=self.password,
                host=self.host,
                database=db
            )
        else:
            conn = connect(
                user=self.user,
                password=self.password,
                host=self.host,
            )
        conn.autocommit = True
        return conn

    def insert_data(self, data):
        sql = f"Insert into task_list VALUES {data}"
        try:
            conn = self.create_connection('tasks', )
            crs = conn.cursor()
            crs.execute(sql)
            crs.close()
            conn.close()
        except OperationalError:
            print('Connection error ')
        except OperationalError:
            print('Connection error ')
